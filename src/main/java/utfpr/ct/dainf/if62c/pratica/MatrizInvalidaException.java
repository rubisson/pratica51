/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Rubinho
 */
public class MatrizInvalidaException extends Exception {
    private int numLinhas;
    private int numColunas;
    private Double[][] matrizA;
    
    public MatrizInvalidaException(){
        
    }
    public MatrizInvalidaException(Double[][] matrizA) throws Exception {
        this.matrizA = matrizA;
        if ((getNumLinhas()<=0)||(getNumColunas()<=0)) throw new Exception("Matriz de " + getNumLinhas() + "x" + getNumColunas() + " não pode ser criada");
        System.out.println("ok!");
    }
    
    
    public int getNumLinhas(){
        return matrizA.length;
    }
    public int getNumColunas(){
        return matrizA[0].length;
    }
 
    
    
    
    
    
    
}

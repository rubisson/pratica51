/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
import utfpr.ct.dainf.if62c.pratica.MatrizInvalidaException;

public class Pratica51{
    public static void main(String[] args) throws Exception {
        Double[][] matriz = new Double[2][];
        try{
            MatrizInvalidaException x=new MatrizInvalidaException(matriz);
        }
        catch (Exception e){
            System.out.println(e);
        }
        finally{
            System.out.println("Finally");
        }
        System.out.println("Fim do Código!");
    }
}
